# Setup

#

In the project root directory

0. (Install Docker Desktop: https://docs.docker.com/desktop/install/mac-install/)
1. Run `docker compose build && docker compose up -d`
2. Run `docker exec -it web bash` to enter the container console
3. Run `composer install && php bin/console doctrine:migrations:migrate -q ` inside the web container
4. Type `exit` to leave the container console
5. Navigate to `http://localhost:8099` in your browser and happy browsing
