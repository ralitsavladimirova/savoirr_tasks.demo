FROM php:8.3-apache

RUN apt-get update \
    && apt-get install -y libzip-dev sqlite3 libsqlite3-dev \
    && docker-php-ext-install zip pdo pdo_sqlite \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY --from=composer /usr/bin/composer /usr/bin/composer

COPY . /var/www/html
ENV APACHE_DOCUMENT_ROOT=/var/www/html/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN a2enmod rewrite

ENV PATH="/usr/local/bin/git:${PATH}"

COPY db-store.sqlite /var/www/html/var/db-store.sqlite
WORKDIR /var/www/html/var
CMD ["sqlite3", "/db-store.sqlite"]

WORKDIR /var/www/html
CMD ["apache2-foreground"]
