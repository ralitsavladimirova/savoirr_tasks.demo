<?php
declare(strict_types=1);

namespace App\Command;

use App\Repository\MemberRepository;
use App\Service\MemberDetailsImporter;
use App\Service\MembersImporter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:import-members', description: 'Import MEP from a configured data source.')]
class ImportMembersCommand extends Command
{
    public function __construct(
        private readonly MembersImporter $membersImporter,
        private readonly MemberDetailsImporter $memberDetailsImporter,
        private readonly MemberRepository $memberRepository,
        private readonly EntityManagerInterface $entityManager,
        ?string $name = null
    )
    {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->entityManager->beginTransaction();
        try {
            $output->writeln('Starting members import. All existing members will be removed.');
            $this->memberRepository->clearAll();

            $output->writeln('Importing members...');
            $this->membersImporter->import();

            $output->writeln('Importing member contact details...');
            foreach ($this->memberRepository->findAll() as $member) {
                $this->memberDetailsImporter->importDetails($member);
            }
            $output->writeln('Members imported successfully.');

            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (\Throwable $e) {
            $this->entityManager->rollback();
            $output->writeln('An error occurred. Changes have been rolled back.');
            throw $e;
        }

        return 0;
    }
}
