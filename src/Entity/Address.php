<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'addresses')]
#[ORM\Entity]
class Address
{
    #[ORM\Id]
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\GeneratedValue]
    private int $id;


    public function __construct(
        #[ORM\ManyToOne(targetEntity: Contact::class, inversedBy: 'addresses')]
        private readonly Contact $contact,
        #[ORM\Column(type: Types::STRING, nullable: false)]
        private readonly string $zipCode,
        #[ORM\Column(type: Types::STRING, nullable: false)]
        private readonly string $city,
        #[ORM\Column(type: Types::STRING, nullable: false)]
        private readonly string $addressLine1,
        #[ORM\Column(type: Types::STRING, nullable: true)]
        private readonly string|null $addressLine2 = null,
        #[ORM\Column(type: Types::STRING, nullable: true)]
        private readonly string|null $addressLine3 = null,
        #[ORM\Column(type: Types::STRING, nullable: true)]
        private readonly string|null $addressLine4 = null
    )
    {
        $this->contact->addAddress($this);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getContact(): Contact
    {
        return $this->contact;
    }

    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getAddressLine1(): string
    {
        return $this->addressLine1;
    }

    public function getAddressLine2(): ?string
    {
        return $this->addressLine2;
    }

    public function getAddressLine3(): ?string
    {
        return $this->addressLine3;
    }

    public function getAddressLine4(): ?string
    {
        return $this->addressLine4;
    }
}
