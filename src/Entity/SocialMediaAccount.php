<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'social_media_accounts')]
#[ORM\Entity]
class SocialMediaAccount
{
    #[ORM\Id]
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\GeneratedValue]
    private int $id;

    public function __construct(
        #[ORM\ManyToOne(targetEntity: Contact::class, inversedBy: 'socialMediaAccounts')]
        private readonly Contact $contact,
        #[ORM\Column(type: Types::STRING, nullable: false)]
        private readonly string $socialMediaName,
        #[ORM\Column(type: Types::STRING, nullable: false)]
        private readonly string $accountUrl,
    )
    {
        $this->contact->addSocialMediaAccount($this);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getContact(): Contact
    {
        return $this->contact;
    }

    public function getSocialMediaName(): string
    {
        return $this->socialMediaName;
    }

    public function getAccountUrl(): string
    {
        return $this->accountUrl;
    }
}
