<?php
declare(strict_types=1);

namespace App\Entity;

enum ContactType: string
{
    case ADDRESS = 'address';
    case EMAIL = 'email';
    case SOCIAL = 'social';
}
