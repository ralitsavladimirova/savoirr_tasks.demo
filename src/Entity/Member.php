<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\MemberRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'members')]
#[ORM\Entity(repositoryClass: MemberRepository::class)]
class Member
{
    #[ORM\Id]
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\GeneratedValue]
    private int $id;

    public function __construct(
        #[ORM\Column(type: Types::INTEGER, unique: true, nullable: false)]
        private readonly int $externalId,
        #[ORM\Column(type: Types::STRING, nullable: false)]
        private readonly string $firstName,
        #[ORM\Column(type: Types::STRING, nullable: false)]
        private readonly string $lastName,
        #[ORM\Column(type: Types::STRING, nullable: false)]
        private readonly string $country,
        #[ORM\Column(type: Types::STRING, nullable: false)]
        private readonly string $politicalGroup,
        #[ORM\Column(type: Types::STRING, nullable: false)]
        private readonly string $nationalPoliticalGroup,
    )
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getExternalId(): int
    {
        return $this->externalId;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function getPoliticalGroup(): string
    {
        return $this->politicalGroup;
    }

    public function getNationalPoliticalGroup(): string
    {
        return $this->nationalPoliticalGroup;
    }
}
