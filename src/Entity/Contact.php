<?php
declare(strict_types=1);

namespace App\Entity;

use App\Repository\ContactRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'contacts')]
#[ORM\Entity(repositoryClass: ContactRepository::class)]
class Contact
{
    #[ORM\Id]
    #[ORM\Column(type: Types::INTEGER)]
    #[ORM\GeneratedValue]
    private int $id;

    #[ORM\OneToMany(targetEntity: Address::class, mappedBy: 'contact', cascade: ['persist'])]
    private Collection $addresses;

    #[ORM\OneToMany(targetEntity: SocialMediaAccount::class, mappedBy: 'contact', cascade: ['persist'])]
    private Collection $socialMediaAccounts;

    public function __construct(
        #[ORM\OneToOne(targetEntity: Member::class)]
        #[ORM\JoinColumn(name: 'member_id', nullable: false)]
        private readonly Member $member,
        #[ORM\Column(type: Types::STRING, nullable: true)]
        private readonly string|null $email = null,
        #[ORM\Column(type: Types::STRING, nullable: true)]
        private readonly string|null $website = null,
    )
    {
        $this->addresses = new ArrayCollection();
        $this->socialMediaAccounts = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(Address $address): self
    {
        $this->addresses->add($address);

        return $this;
    }

    public function getSocialMediaAccounts(): Collection
    {
        return $this->socialMediaAccounts;
    }

    public function addSocialMediaAccount(SocialMediaAccount $socialMediaAccount): self
    {
        $this->socialMediaAccounts->add($socialMediaAccount);

        return $this;
    }
}
