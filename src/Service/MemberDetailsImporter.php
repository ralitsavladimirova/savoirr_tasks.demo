<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Address;
use App\Entity\Contact;
use App\Entity\Member;
use App\Entity\SocialMediaAccount;
use App\Repository\ContactRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use function str_replace;
use function strtolower;

class MemberDetailsImporter
{
    public function __construct(
        private readonly HttpClientInterface $client,
        private readonly EntityManagerInterface $entityManager,
        private readonly ContactRepository $contactRepository,
        private readonly string $sourceUrlTemplate
    )
    {
    }

    public function importDetails(Member $member): void
    {
        $existingContact = $this->contactRepository->findOneBy(['member' => $member]);
        if ($existingContact !== null) {
            $this->entityManager->remove($existingContact);
            $this->entityManager->flush();
        }

        $crawler = $this->getCrawler($member);

        foreach ($crawler->filterXPath("//div[@class='erpl_contact-card-list']") as $domElement) {
            // TODO: Get contact details - failed
        }


        foreach ($crawler->filterXPath("//a[starts-with(@class, 'link_')]") as $domElement) {
            // TODO: Get social media accounts - failed
        }

        // So here's some sample data
        $contact = new Contact(
            member: $member,
            email: 'demo_static@nonexisting.com',
            website: 'https://www.nonexisting.com'
        );

        $socialMediaAccount1 = new SocialMediaAccount(
            $contact,
            'nonexisting_' . strtolower($member->getFirstName()) . '_' . strtolower($member->getLastName()),
            'Twitter'
        );
        $socialMediaAccount2 = new SocialMediaAccount(
            $contact,
            'nonexisting_' . strtolower($member->getFirstName()) . '_' . strtolower($member->getLastName()),
            'Instagram'
        );

        $address1 = new Address(
            $contact,
            'B-12345',
            'Brussels',
            'Parlement européen',
            'Building 2',
            '123, Non-existent Str.'
        );
        $address2 = new Address(
            $contact,
            'F-12345',
            'Strasbourg',
            'Parlement européen',
            '555, Non-existent Str.'
        );

        $this->entityManager->persist($contact);
        $this->entityManager->flush();
    }

    private function getCrawler(Member $member): Crawler
    {
        $targetUrl = str_replace(
            ['member_id', 'member_name'],
            [$member->getExternalId(), $member->getFirstName() . '_' . $member->getLastName()],
            $this->sourceUrlTemplate
        );
        $response = $this->client->request('GET', $targetUrl);
        $htmlContent = $response->getContent();

        return new Crawler($htmlContent);
    }
}
