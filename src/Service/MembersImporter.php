<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Member;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class MembersImporter
{
    public function __construct(
        private readonly HttpClientInterface $client,
        private readonly EntityManagerInterface $entityManager,
        private readonly string $sourceUrl
    )
    {
    }

    public function import(): void
    {
        $response = $this->client->request('GET', $this->sourceUrl);
        $xmlContent = $response->getContent();

        $crawler = new Crawler($xmlContent);
        foreach ($crawler->filterXPath('//mep') as $mepElement) {
            $mepCrawler = new Crawler($mepElement);
            list($firstName, $lastName) = explode(' ', $mepCrawler->filterXPath('//fullName')->text(), 2);

            $member = new Member(
                externalId: (int)$mepCrawler->filterXPath('//id')->text(),
                firstName: $firstName,
                lastName: $lastName,
                country: $mepCrawler->filterXPath('//country')->text(),
                politicalGroup: $mepCrawler->filterXPath('//politicalGroup')->text(),
                nationalPoliticalGroup: $mepCrawler->filterXPath('//nationalPoliticalGroup')->text(),
            );

            $this->entityManager->persist($member);
        }
        $this->entityManager->flush();
    }
}
