<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\Member;
use App\Repository\MemberRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

class DefaultController extends AbstractController
{
    public function index(): Response
    {
        return new Response('LIST: /api/members </br> DETAILS: /api/member/{id}');
    }
}
