<?php
declare(strict_types=1);

namespace App\Controller;

use App\Repository\ContactRepository;
use App\Repository\MemberRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends AbstractController
{
    public function __construct(private readonly MemberRepository $memberRepository)
    {
    }


    public function listMembers(): JsonResponse
    {
        $members = $this->memberRepository->findAll();

        $data = [];
        foreach ($members as $member) {
            $data[] = [
                'id' => $member->getExternalId(),
                'firstName' => $member->getFirstName(),
                'lastName' => $member->getLastName(),
                'country' => $member->getCountry(),
                'politicalGroup' => $member->getPoliticalGroup(),
                'nationalPoliticalGroup' => $member->getNationalPoliticalGroup()
            ];
        }

        return $this->json($data);
    }

    public function getMember(int $memberId, ContactRepository $contactRepository): JsonResponse
    {
        $member = $this->memberRepository->findOneBy(['externalId' => $memberId]);
        if (null === $member) {
            return $this->json(['error' => 'Member not found'], Response::HTTP_NOT_FOUND);
        }

        $contactEntry = $contactRepository->findOneBy(['member' => $member]);

        $contacts = [];
        if (null !== $contactEntry) {
            $contacts[] = [
                'type' => 'email',
                $contactEntry->getEmail(),
            ];

            foreach ($contactEntry->getAddresses() as $address) {
                $contacts[] = [
                    'type' => 'address',
                    'value' => $address->getZipCode() . ' ' . $address->getCity() . ' ' . $address->getAddressLine1() . ' ' . $address->getAddressLine2() . ' ' . $address->getAddressLine3() . ' ' . $address->getAddressLine4(),
                ];
            }

            foreach ($contactEntry->getSocialMediaAccounts() as $socialMediaAccount) {
                $contacts[] = [
                    'type' => 'social',
                    'value' => $socialMediaAccount->getAccountUrl(),
                ];
            }
        }

        return $this->json([
            'id' => $member->getId(),
            'firstName' => $member->getFirstName(),
            'lastName' => $member->getLastName(),
            'country' => $member->getCountry(),
            'politicalGroup' => $member->getPoliticalGroup(),
            'nationalPoliticalGroup' => $member->getNationalPoliticalGroup(),
            'contacts' => $contacts,

        ]);
    }
}
