<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240204105409 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create members table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE members (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, external_id INTEGER NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, political_group VARCHAR(255) NOT NULL, national_political_group VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_45A0D2FF9F75D7B0 ON members (external_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE members');
    }
}
