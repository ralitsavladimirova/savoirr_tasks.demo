<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240204133414 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add member contacts and social media accounts tables.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE addresses (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, zip_code VARCHAR(255) NOT NULL, city VARCHAR(255) NOT NULL, address_line1 VARCHAR(255) NOT NULL, address_line2 VARCHAR(255) DEFAULT NULL, address_line3 VARCHAR(255) DEFAULT NULL, address_line4 VARCHAR(255) DEFAULT NULL, contact_id INTEGER DEFAULT NULL, CONSTRAINT FK_6FCA7516E7A1254A FOREIGN KEY (contact_id) REFERENCES contacts (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_6FCA7516E7A1254A ON addresses (contact_id)');
        $this->addSql('CREATE TABLE contacts (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email VARCHAR(255) DEFAULT NULL, website VARCHAR(255) DEFAULT NULL, member_id INTEGER NOT NULL, CONSTRAINT FK_334015737597D3FE FOREIGN KEY (member_id) REFERENCES members (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_334015737597D3FE ON contacts (member_id)');
        $this->addSql('CREATE TABLE social_media_accounts (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, social_media_name VARCHAR(255) NOT NULL, account_url VARCHAR(255) NOT NULL, contact_id INTEGER DEFAULT NULL, CONSTRAINT FK_327D5DCDE7A1254A FOREIGN KEY (contact_id) REFERENCES contacts (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('CREATE INDEX IDX_327D5DCDE7A1254A ON social_media_accounts (contact_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE addresses');
        $this->addSql('DROP TABLE contacts');
        $this->addSql('DROP TABLE social_media_accounts');
    }
}
